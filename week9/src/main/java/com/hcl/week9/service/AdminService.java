package com.hcl.week9.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.week9.entity.Admin;
import com.hcl.week9.repository.IAdminRepository;

@Service
public class AdminService {
	
	@Autowired
	IAdminRepository adminrepo;
	
	public String login(Admin admin) {
		   if(adminrepo.existsById(admin.getMail())) {
			   Admin ad = adminrepo.getById(admin.getMail());
			   if(ad.getPassword().equals(admin.getPassword())) {
				   return "Successfully logged in";
			   }else {
				   return "Incorrect details";
			   }				   
		   }else {
			   return "Your details are not added..check again";
		   }
		   
	   }
	public String logout(String mail) {
		if(!adminrepo.existsById(mail)) {
			return "Incorrect details ";
		}else {
			adminrepo.deleteById(mail);
			return "Successfully logged out";
		}
	}
	public List<Admin> getAllAdmins(){
		return adminrepo.findAll();
	}
	 public String registration(Admin admin) {
		   if(adminrepo.existsById(admin.getMail())) {
			   return "Registered already";
		   }else {
			   adminrepo.save(admin);
			   return "Sucessfully registered";
		   }	   
	   }
}
