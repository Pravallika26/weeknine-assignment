package com.hcl.week9.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.week9.entity.ReadLater;
import com.hcl.week9.repository.IReadLaterRepository;

@Service
public class ReadLaterService {
	
	@Autowired
	IReadLaterRepository readlaterrepo;
	
	public String addReadLater(ReadLater later) {
		if (readlaterrepo.existsById((later.getBookId()))) {
			return "Id must be distinct";
		} else {
			readlaterrepo.save(later);
			return "Book added to readlater list";
		}
	}

	public List<ReadLater> getLater() {
		return readlaterrepo.findAll();
	}

	public Optional<ReadLater> getLaterById(int bookid) {
		return readlaterrepo.findById(bookid);
	}

	public String deleteLater(int bookid) {
		if (!readlaterrepo.existsById(bookid)) {
			return "Book is not added in the readlater list";
		} else {
			readlaterrepo.deleteById(bookid);
			return "Successfully deleted from readlater list";
		}

	}

}
