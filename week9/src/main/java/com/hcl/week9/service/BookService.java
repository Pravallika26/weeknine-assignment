package com.hcl.week9.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.week9.entity.Book;
import com.hcl.week9.repository.IBookRepository;

@Service
public class BookService {
	
	@Autowired
	IBookRepository bookrepo;

	public String saveBook(Book book) {
		if (bookrepo.existsById(book.getBookId())) {
			return "Id must be distinct";
		} else {
			bookrepo.save(book);
			return "Successfully saved";
		}
	}
	
	public List<Book> getAllBooks() {
		return bookrepo.findAll();
	}

	public Optional<Book> getBookById(int bookid ) {
		return bookrepo.findById(bookid);
	}
	
	public String updateBook(Book book) {
		if (!bookrepo.existsById(book.getBookId())) {
			return "Book not Found";
		} else {
			Book upb = bookrepo.getById(book.getBookId());
			upb.setTitle(book.getTitle());
			bookrepo.saveAndFlush(upb);
			return "Successfully updated";
		}
	}
	
	public String deleteBook(int bookid) {
		if (!bookrepo.existsById(bookid)) {
			return "Book is not added";
		} else {
			bookrepo.deleteById(bookid);
			return "Successfully deleted";
		}
	}
	
}
