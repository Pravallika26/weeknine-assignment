package com.hcl.week9.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.week9.entity.User;
import com.hcl.week9.repository.IUserRepository;

@Service
public class UserService {
	
	@Autowired
	IUserRepository userrepo;

	public String saveUser(User user) {
		if(!userrepo.existsById(user.getMail())) {
			userrepo.save(user);
			return "Sucessfully added";
		}else {
			return "Your details added already";
		}
	}
	
	public List<User> getAllUsers() {
		return userrepo.findAll();
	}
	
	public String updateUser(User user) {
		if(userrepo.existsById(user.getMail())) {
			User upuser = userrepo.getById(user.getMail());
			if(upuser.getPassword().equals(user.getPassword())) {
				return "Please provide new details";
			}else {
				upuser.setPassword(user.getPassword());
				userrepo.saveAndFlush(upuser);
				return "Sucessfully updated";
			}
		}else {
			return "Your details are not there";			
		}						
	}
	
	public String deleteUser(String email) {
		if(!userrepo.existsById(email)) {
			return "Your details are not there";
		}else {
			userrepo.deleteById(email);
			return "Sucessfully deleted";
		}
	}
}
