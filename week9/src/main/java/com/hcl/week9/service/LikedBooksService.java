package com.hcl.week9.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.week9.entity.LikedBooks;
import com.hcl.week9.repository.ILikedBooksRepository;

@Service
public class LikedBooksService {

	@Autowired
	ILikedBooksRepository likedrepo;

	public String addLiked(LikedBooks liked) {
		if (likedrepo.existsById((liked.getBookId()))) {
			return "Id must be distinct";
		} else {
			likedrepo.save(liked);
			return "Book added to liked list";
		}
	}

	public List<LikedBooks> getLiked() {
		return likedrepo.findAll();
	}

	public Optional<LikedBooks> getLikedById(int bookid) {
		return likedrepo.findById(bookid);
	}

	public String deleteLiked(int bookid) {
		if (!likedrepo.existsById(bookid)) {
			return "Book is not added in the liked list";
		} else {
			likedrepo.deleteById(bookid);
			return "Successfully deleted from liked list";
		}

	}
}