package com.hcl.week9.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.week9.entity.Admin;

@Repository
public interface IAdminRepository extends JpaRepository<Admin, String>{

}
