package com.hcl.week9.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.week9.entity.ReadLater;
import com.hcl.week9.service.ReadLaterService;

@RestController
@RequestMapping(value = "/later")
public class ReadLaterController {
	
	@Autowired
	ReadLaterService readlaterservice;
	
	@PostMapping(value = "/addLater", consumes = MediaType.APPLICATION_JSON_VALUE)
	public String addBook(@RequestBody ReadLater later) {
		return readlaterservice.addReadLater(later);
	}

	@GetMapping(value = "/getLater")
	public List<ReadLater> getAllBooks() {
		return readlaterservice.getLater();
	}

	@GetMapping(value = "/getLaterById/{bookid}")
	public Optional<ReadLater> getBookById(@PathVariable("bookid") int bookid) {
		return readlaterservice.getLaterById(bookid);
	}

	@DeleteMapping(value = "/deleteLaterById/{bookid}")
	public String deleteLater(@PathVariable("bookid") int bookid) {
		return readlaterservice.deleteLater(bookid);
	}

}
