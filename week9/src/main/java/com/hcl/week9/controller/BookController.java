package com.hcl.week9.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.week9.entity.Book;
import com.hcl.week9.service.BookService;

@RestController
@RequestMapping(value = "/book")
public class BookController {

	@Autowired
	BookService bookservice;

	@PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE)
	public String addBook(@RequestBody Book book) {
		return bookservice.saveBook(book);
	}

	@GetMapping(value = "/getAll")
	public List<Book> getAllBooks() {
		return bookservice.getAllBooks();
	}

	@GetMapping(value = "/getBookById/{bookid}")
	public Optional<Book> getBookById(@PathVariable("bookid") int bookid) {
		return bookservice.getBookById(bookid);
	}

	@PatchMapping(value = "/update")
	public String updateBook(@RequestBody Book book) {
		return bookservice.updateBook(book);
	}

	@DeleteMapping(value = "/deleteById/{bookid}")
	public String deleteBook(@PathVariable("bookid") int bookid) {
		return bookservice.deleteBook(bookid);
	}

}
