package com.hcl.week9.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.week9.entity.LikedBooks;
import com.hcl.week9.service.LikedBooksService;

@RestController
@RequestMapping(value = "/liked")
public class LikedBooksController {
	
	@Autowired
	LikedBooksService likedservice;
	
	@PostMapping(value = "/addLiked", consumes = MediaType.APPLICATION_JSON_VALUE)
	public String addBook(@RequestBody LikedBooks liked) {
		return likedservice.addLiked(liked);
	}

	@GetMapping(value = "/getLiked")
	public List<LikedBooks> getAllBooks() {
		return likedservice.getLiked();
	}

	@GetMapping(value = "/getLikedById/{bookid}")
	public Optional<LikedBooks> getBookById(@PathVariable("bookid") int bookid) {
		return likedservice.getLikedById(bookid);
	}

	@DeleteMapping(value = "/deleteLikedById/{bookid}")
	public String deleteLiked(@PathVariable("bookid") int bookid) {
		return likedservice.deleteLiked(bookid);
	}
}
