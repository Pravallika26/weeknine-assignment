package com.hcl.week9.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.week9.entity.Admin;
import com.hcl.week9.service.AdminService;

@RestController
@RequestMapping(value = "/admin")
public class AdminController {

	@Autowired
	AdminService adminservice;
	
	@PatchMapping(value="/login")
	public String login(@RequestBody Admin admin) {
		return adminservice.login(admin);
		
	}
	
	@GetMapping(value="/logoutByMail/{mail}")
	public String logout(@PathVariable("mail") String mail) {
		return adminservice.logout(mail);
	}
	
	@GetMapping(value="/info")
	public List<Admin> getAllAdmins() {
		return adminservice.getAllAdmins();
	}
	
	@PostMapping(value="/register",consumes=MediaType.APPLICATION_JSON_VALUE)
	public String registration(@RequestBody Admin admin) {
		return adminservice.registration(admin);
	}
}
