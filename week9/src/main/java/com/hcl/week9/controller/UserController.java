package com.hcl.week9.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.week9.entity.User;
import com.hcl.week9.service.UserService;

@RestController
@RequestMapping(value = "/user")
public class UserController {

	@Autowired
	UserService userservice;
	
	@PostMapping(value="/add",consumes=MediaType.APPLICATION_JSON_VALUE)
	public String addUser(@RequestBody User user) {
		return userservice.saveUser(user);
		
	}
	
	@GetMapping(value="/getAll")
	public List<User> getAllUsers(){
		return userservice.getAllUsers();
	}
	
	@PatchMapping(value="/updatePassword")
	public String updateUser(@RequestBody User user) {
		return userservice.updateUser(user);
	}
	
	@DeleteMapping(value="/deleteByMail/{mail}")
	public String deleteUser(@PathVariable("mail") String mail) {
		return userservice.deleteUser(mail);
	}
}
